import { connect } from 'react-redux'
import Component from '../components/PlayerMoveComp'

const mapStateToProps = (state, ownProps) => ({
  player: state.players[ownProps.match.params.player],
  round: state.roundIndex,
  moves: state.moves,
  players: state.players,
  winner: state.winner
})

const PlayerMove = connect(
  mapStateToProps
)(Component)

export default PlayerMove