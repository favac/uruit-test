import React from 'react'

const InfoRound = (props) => (
  <div className="alert alert-info">
    <h1>Round {props.round}</h1>
    <h4>Results so far: { props.players.player1 }: {props.winner.player1Score || 0} - { props.players.player2 }: { props.winner.player2Score || 0}</h4>
  </div>
)

export default InfoRound