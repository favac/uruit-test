const playersMove = (state = {}, action) => {
  switch (action.type) {
    case 'SET_PLAYER_MOVE':
      return Object.assign({}, state, {[action.player]: action.move})
    default:
      return state
  }
}
export default playersMove