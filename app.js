const express = require('express')
const bodyParser = require("body-parser")
const low = require('lowdb')
const _ = require("lodash")
const path = require('path')

const app = express()

app.use(bodyParser.json())

// database (in memory)
const db = low()

const initPlayers = {
  player1: "", player2: ""
}

const data = {
  moves: [
    { id: "paper", name: "Paper" },
    { id: "rock", name: "Rock" },
    { id: "scissors", name: "Scissors" }
  ],
  movesWin: [
    { id: 1, move1: "paper", move2: "scissors", win: "scissors" },
    { id: 2, move1: "paper", move2: "rock", win: "paper" },
    { id: 3, move1: "rock", move2: "scissors", win: "rock" }
  ],
  rounds: [],
  players: [initPlayers]
}

// initial data
db.defaults(data).write()

// routes
app.get("/api/v1/state", (req, res) => {
  let state = db.getState()
  res.send(state)
})

app.get("/api/v1/moves", (req, res) => {
  let moves = db.get("moves").value()
  res.send(moves)
})

app.post("/api/v1/moves", (req, res) => {
  let move = req.body
  if (!move.name) {
    res.status(400).send("Error: Should define the name of move")
    return
  }
  let id = _.snakeCase(move.name)
  let newMove = {id: id, name: move.name}
  db.get("moves").push(newMove).write()
  res.send(newMove)
})

app.delete("/api/v1/moves/:id", (req, res) => {
  let id = req.params.id
  if (!id) {
    res.status(400).send("Error: Should define the id to remove")
    return
  }
  db.get("moves").remove({id: id}).write()
  res.json({message: id})
})

app.get("/api/v1/movesWin", (req, res) => {
  let movesWin = db.get("movesWin").map(f => {
    return {
      id: f.id,
      move1: f.move1,
      move1Name: db.get("moves").find({id: f.move1}).value().name,
      move2: f.move2,
      move2Name: db.get("moves").find({id: f.move2}).value().name,
      win: f.win,
      winName: db.get("moves").find({id: f.win}).value().name,
    }
  }).value()
  res.send(movesWin)
})

app.post("/api/v1/movesWin", (req, res) => {
  let move = req.body
  if (!move.move1 || !move.move2 || !move.win) {
    res.status(400).send("Error: Must define properties move1, move2 and win")
    return
  }
  if (
    move.move1.trim() === "" ||
    move.move2.trim() === "" ||
    move.win.trim() === ""
  ) {
    res.status(400).send("Error: Must define values for properties move1, move2 and win")
    return
  }
  let id = db.get("movesWin").map("id").max().value()
  move.id = id + 1
  db.get("movesWin").push(move).write()
  let newMove = {
      id: move.id,
      move1: move.move1,
      move1Name: db.get("moves").find({id: move.move1}).value().name,
      move2: move.move2,
      move2Name: db.get("moves").find({id: move.move2}).value().name,
      win: move.win,
      winName: db.get("moves").find({id: move.win}).value().name,
    }
  res.send(newMove)
})

app.delete("/api/v1/movesWin/:id", (req, res) => {
  let id = req.params.id
  if (!id) {
    res.status(400).send("Error: Should define and id to delete")
  }
  db.get("movesWin").remove({id: parseInt(id)}).write()
  res.send(id)
})

app.get("/api/v1/players", (req, res) => {
  let players = db.get("players").first().value()
  res.send(players)
})


// set players name
app.post("/api/v1/player", (req, res) => {
  let players = req.body;
  if (players.player1.trim() === "" || players.player2.trim() === "") {
    res.status(400).json({message: "Error: Should assign a player name", status: res.status})
  }
  // update existing player
  db.set("players", [players]).write()
  res.send(db.get("players").value()[0])
})


// Return the winner for the match:
// req.query:
// - player1: String. Move for player1
// - player2: String. Move for player2
// - round: round number
// return:
// { player: String, move: String, playerName: String, round: number }
// in case of tie return { player: null, move: null, playerName: "", round: number }
app.get("/api/v1/winner", (req, res) => {
  if (!req.query.player1 || !req.query.player2 || !req.query.round) {
    res.status(400).send("Error: Should set player1, player2 and roun as query parameters")
    return
  }
  let player1 = req.query.player1
  let player2 = req.query.player2
  // returning object
  // get rounds won by player1
  let p1Sc = db.get("rounds").filter({winner:"player1"}).size().value()
  // get rounds won by player2
  let p2Sc = db.get("rounds").filter({winner:"player2"}).size().value()

  let winObj = {
    player: null,
    playerName: "",
    move: null,
    round: req.query.round,
    player1Score: p1Sc,
    player2Score: p2Sc
  }

  // find the winning move pair
  let winMove = db.get("movesWin").find({move1: player1, move2: player2}).value()
  if (!winMove) winMove = db.get("movesWin").find({move1: player2, move2: player1}).value()

  // no winner found
  if (!winMove) { res.send(winObj); return; }

  // get player names for winner
  let winningPlayer = "", winningPlayerName = "";

  if (winMove.win === player1) {
    winningPlayer = "player1";
    winningPlayerName = db.get("players").value()[0].player1
  } else {
    winningPlayer = "player2";
    winningPlayerName = db.get("players").value()[0].player2
  }
  // update round list
  db.get("rounds")
    .push({round: req.query.round, winner: winningPlayer, playerName: winningPlayerName})
    .write()

  // get rounds won by player1
  let p1Score = db.get("rounds").filter({winner:"player1"}).size().value()

  // get rounds won by player2
  let p2Score = db.get("rounds").filter({winner:"player2"}).size().value()

  winObj = {
    player: winningPlayer,
    playerName: winningPlayerName,
    move: winMove.win,
    round: req.query.round,
    player1Score: p1Score,
    player2Score: p2Score
  }
  res.send(winObj)
})

app.get("/api/v1/rounds", (req, res) =>{
  let rounds = db.get("rounds").value()
  res.send(rounds)
})

app.post("/api/v1/reset", (req, res) => {
  db.set("rounds", []).write()
  // db.set("players", [{ player1: "", player2: ""}]).write()
  res.json({message: "ok"})
})


// Serve static assets
app.use(express.static(path.resolve(__dirname, 'client/build')))

// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
});

module.exports = app;

