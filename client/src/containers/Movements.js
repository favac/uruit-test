import React from 'react'
import { connect } from 'react-redux'
import {
  fetchMoves,
  setMove,
  removeMove,
  fetchMovesWin,
  setWinConfiguration
} from '../actions'

class Movements extends React.Component {
  componentDidMount () {
    this.props.dispatch(fetchMoves())
    this.props.dispatch(fetchMovesWin())
  }
  onSubmit (e) {
    e.preventDefault()
    this.props.dispatch(setMove(this.refs.move.value))
    this.refs.move.value = ""
    // this.props.dispatch(fetchMoves())
  }
  removeMode (e, id) {
    e.preventDefault()
    this.props.dispatch(removeMove(id))
  }
  onAddWinConfiguration (e) {
    e.preventDefault()
    this.props.dispatch(setWinConfiguration(
      {
        move1: this.refs.move1.value,
        move2: this.refs.move2.value,
        win: this.refs.win.value
      }
    ))
  }
  render () {
    return (
      <div>
        <h1>Moves Configurator</h1>
        <div className="row">
          <div className="col-md-6">
            <h3>Current Moves</h3>
            <table className="table table-hover">
              <tbody>
                { this.props.moves.map(m => (
                  <tr key={m.id}>
                    <td>{m.name}</td>
                    <td><a href="" onClick={ e => this.removeMode(e, m.id) }>Remove</a></td>
                  </tr> )
                )}
              </tbody>
            </table>
          </div>
          <div className="col-md-6">
            <h3>Add Movement</h3>
            <form onSubmit = { e => this.onSubmit(e) }>
              <div className="form-group">
                <label htmlFor="move" className="control-label">New Move:</label>
                <input type="text" className="form-control" ref="move"/>
              </div>
              <button type="submit" className="btn btn-success">Add</button>
            </form>
          </div>
        </div>
        <hr/>
        <div className="row">
          <div className="col-md-6">
            <h2>Win Configuration</h2>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>Move 1</th>
                  <th>Move 2</th>
                  <th>Winner</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                { this.props.movesWin.map(m => (
                  <tr key={m.id}>
                    <td>{m.move1Name}</td>
                    <td>{m.move2Name}</td>
                    <td>{m.winName}</td>
                    <td><a href="" onClick={ e => this.removeModeWin(e, m.id) }>Remove</a></td>
                  </tr> )
                )}
              </tbody>
            </table>
          </div>
          <div className="col-md-6">
            <h2>Add Configuration</h2>
            <form onSubmit = { e => this.onAddWinConfiguration(e) }>
              <div className="form-group">
                <label htmlFor="move1" className="control-label">Move 1</label>
                <select className="form-control" ref="move1">
                  { this.props.moves.map(m => (
                  <option value={m.id}>{m.name}</option> )
                )}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="move2" className="control-label">Move 1</label>
                <select className="form-control" ref="move2">
                  { this.props.moves.map(m => (
                  <option value={m.id}>{m.name}</option> )
                )}
                </select>
              </div>

              <div className="form-group">
                <label htmlFor="move2" className="control-label">Winner</label>
                <select className="form-control" ref="win">
                  { this.props.moves.map(m => (
                  <option value={m.id}>{m.name}</option> )
                )}
                </select>
              </div>

              <button type="submit" className="btn btn-success">Add</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  moves: state.moves,
  movesWin: state.movesWin
})

Movements = connect(mapStateToProps)(Movements)

export default Movements