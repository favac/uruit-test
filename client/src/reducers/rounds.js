const initialState = 0

const roundIndex = (state = initialState, action) => {
  switch (action.type) {
    case 'INCREMENT_ROUND':
      return action.roundIndex + 1
    case 'RESET_ROUND':
      return action.roundIndex
    default:
      return state
  }
}

export default roundIndex