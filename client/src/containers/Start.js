import React from 'react';
import PlayersForm from '../containers/PlayersForm'
import { connect } from 'react-redux'
import { resetRound, reset } from '../actions'

class Start extends React.Component {
  componentDidMount () {
    this.props.dispatch(resetRound())
    this.props.dispatch(reset())
  }
  render () {
    return (
      <div>
        <div className="jumbotron">
          <h1>Game of Drones</h1>
          <p>UruIT Assesment - Felix Vargas</p>
        </div>
        <div className="row">
          <div className="col-md-12 alert alert-success">
            This is just like the game “Paper, rock, scissors”. Set the name of players below to start the game!
          </div>
        </div>
        <PlayersForm />
      </div>
    )
  }
}

Start = connect()(Start)

export default Start;
