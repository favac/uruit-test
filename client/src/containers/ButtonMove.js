import { connect } from 'react-redux'
import Component from '../components/ButtonMoveComp'
import { setPlayerMove } from '../actions'

const mapDispatchToProps = (dispatch) => ({
  onClick: (player, move) => {
    dispatch(setPlayerMove(player, move))
  }
})

const mapStateToProps = (state) => ({
  round: state.roundIndex
})

const ButtonMove = connect(mapStateToProps, mapDispatchToProps)(Component)

export default ButtonMove