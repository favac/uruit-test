import React from 'react'
import { connect } from 'react-redux'
import { fetchWinner, incrementRound } from '../actions'
import InfoRound from '../components/InfoRound'

class RoundResult extends React.Component {
  componentDidMount () {
    this.props.dispatch(fetchWinner({ ...this.props.playersMove, round: this.props.round}))
  }
  continuar () {
    this.props.dispatch(incrementRound(this.props.round))
    this.props.history.push("/playerMove/player1")
  }
  render () {
    const newEmperor = () => {
      return this.props.winner.player1Score===3?"player1":this.props.winner.player2Score===3?"player2":null
    }
    return (
      <div>
        <div className="row">
          <div className="col-md-12">
            <InfoRound winner={this.props.winner} players={this.props.players} round={this.props.round}/>
            <div className="row">
              <div className="col-md-4 col-xs-1"></div>
              <div className="col-md-4 col-xs-10" style={{textAlign:'center'}}>
                <h3>{this.props.players.player1} Move:</h3>
                <div className="alert alert-warning">
                  <p>{this.props.playersMove.player1}</p>
                </div>
                <h3>{this.props.players.player2} Move:</h3>
                <div className="alert alert-warning">
                  <p>{this.props.playersMove.player2}</p>
                </div>
                <h3>Result: </h3>
                { !newEmperor() &&
                  (
                  <div>
                    <div className="alert alert-success">
                      {this.props.winner.player && (
                        <h2>{this.props.winner.playerName} Wins!!!</h2>
                      )}
                      {!this.props.winner.player && (
                        <h2>It's a Tie</h2>
                      )}
                    </div>
                    <button className="btn btn-block btn-success" onClick={ e => this.continuar(e) }>Continuar</button>
                  </div>
                  )
                }
                { newEmperor() &&
                  (<div className="alert alert-danger">
                    <h2>{this.props.winner.playerName} Is the new Emperor!!!</h2>
                  </div>)
                }
              </div>
              <div className="col-md-4 col-xs-1"></div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  playersMove: state.playersMove,
  players: state.players,
  round: state.roundIndex,
  winner: state.winner
})

RoundResult = connect(mapStateToProps)(RoundResult)

export default RoundResult