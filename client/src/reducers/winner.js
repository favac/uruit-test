const winner = (state = {}, action) => {
  switch (action.type) {
    case 'REQUEST_WINNER':
      return state
    case 'RECEIVE_WINNER':
      return action.winner
    default:
      return state
  }
}

export default winner
