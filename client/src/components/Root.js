import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route} from 'react-router-dom'
import NavBar from './NavBar'


import Start from '../containers/Start'
import PlayerMove from '../containers/PlayerMove'
import RoundResult from '../containers/RoundResult'
import Movements from '../containers/Movements'

const Root = ({store}) => (
  <Provider store = {store}>
    <Router>
      <div>
        <NavBar />
        <div className="container" style={{marginTop:60}}>
          <Route exact path="/" component={ Start } />
          <Route path="/playerMove/:player" component={ PlayerMove } push/>
          <Route path="/roundResult" component={ RoundResult } push/>
          <Route path="/movements" component={ Movements } push/>
        </div>
      </div>
    </Router>
  </Provider>
)

export default Root