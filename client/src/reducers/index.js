import { combineReducers } from 'redux'
import players from './players'
import roundIndex from './rounds'
import moves from './moves'
import playersMove from './playersMove'
import winner from './winner'
import movesWin from './movesWin'

const rootReducer = combineReducers({
  players,
  roundIndex,
  moves,
  playersMove,
  winner,
  movesWin
})

export default rootReducer
