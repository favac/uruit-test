export const sendPlayers = (player1, player2) => ({
  type: "SEND_PLAYERS",
  player1,
  player2
})

export const receivePlayers = (objPlayers) => ({
  type: "RECEIVE_PLAYERS",
  player1: objPlayers.player1,
  player2: objPlayers.player2
})

export const receivePlayersError = (error) => ({
  type: "RECEIVE_PLAYERS_ERROR",
  error
})

export const setPlayers  = (player1, player2) => {
  return (dispatch) => {
    dispatch(sendPlayers(player1, player2))
    return fetch('/api/v1/player',
    {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({player1: player1, player2: player2})
    })
    .then(response => {
      response.json()
      .then(json => {
        if (response.status === 200)
          dispatch(receivePlayers(json))
        else
          dispatch(receivePlayersError(json.message))
      })
    })
  }
}

const roundIndex = 0;
export const incrementRound = (round = roundIndex) => ({
  type: "INCREMENT_ROUND",
  roundIndex: round
})

export const resetRound = () => ({
  type: "RESET_ROUND",
  roundIndex: 0
})

export const requestMoves = () => ({
  type: "REQUEST_MOVES",
  moves: []
})

export const receiveMoves = (moves) => ({
  type: "RECEIVE_MOVES",
  moves
})

export const fetchMoves = () => {
  return function (dispatch) {
    dispatch(requestMoves())
    return fetch('/api/v1/moves')
    .then(response => response.json())
    .then(json => dispatch(receiveMoves(json)))
  }
}

export const settedMove = (move) => ({
  type: "SETTED_MOVE",
  move: move
})

export const setMove = (move) => {
  return function (dispatch) {
    return fetch("/api/v1/moves", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({name: move})
    })
    .then(response => response.json())
    .then(json => dispatch(settedMove(json)))
  }
}

export const removedMove = (obj) => ({
  type: "REMOVED_MOVE",
  id: obj.message
})

export const removeMove = (id) => {
  return function (dispatch) {
    return fetch("/api/v1/moves/"+id, {
      method: "DELETE",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then(response => response.json())
    .then(json => {dispatch(removedMove(json))})
  }
}

export const receivedMovesWin = (moves) => ({
  type: "RECEIVED_MOVES_WIN",
  moves
})

export const fetchMovesWin = () => {
  return function (dispatch) {
    return fetch("/api/v1/movesWin/")
    .then(response => response.json())
    .then(json => {dispatch(receivedMovesWin(json))})
  }
}

export const settedMoveWin = (obj) => ({
  type: "SETTED_MOVE_WIN"
  , moveWin: obj
})

export const setWinConfiguration = (conf) => {
  return function (dispatch) {
    return fetch("/api/v1/movesWin", {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(conf)
    })
    .then(response => response.json())
    .then(json => dispatch(settedMoveWin(json)))
  }
}

export const setPlayerMove = (player, move) => ({
  type: "SET_PLAYER_MOVE",
  player,
  move
})

export const requestWinner = () => ({
  type: "REQUEST_WINNER",
  winner: {}
})

export const receiveWinner = (obj) => ({
  type: "RECEIVE_WINNER",
  winner: obj
})

export const fetchWinner = (objMoves) => {
  return (dispatch) => {
    dispatch(requestWinner())
    return fetch(`/api/v1/winner?player1=${objMoves.player1}&player2=${objMoves.player2}&round=${objMoves.round}`)
    .then(response => response.json())
    .then(json => dispatch(receiveWinner(json)))
  }
}

export const infoReseted = (info) => ({
  type: "INFO_RESETED",
  result: info.message
})

export const reset = () => {
  return (dispatch) => {
    return fetch('/api/v1/reset',
    {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then(response => response.json())
    .then(json => dispatch(infoReseted(json)))
  }
}