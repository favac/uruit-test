import React from 'react'

const ButtonMove = ({move, onClick, playerId, history}) => (
  <button className="btn btn-default btn-lg btn-block" onClick = { e => {
      onClick(playerId, move.id)
      if (playerId === "player1") history.push("/playerMove/player2")
      if (playerId === "player2") history.push("/roundResult")
    }
  }>
    {move.name}
  </button>
)

export default ButtonMove