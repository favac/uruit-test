import { connect } from 'react-redux'
import ErrorAlert from '../components/ErrorAlert'

const mapStateToProps = (state, ownProps) => ({
  error: state.players.error
})

const PlayersFormError = connect(
  mapStateToProps
)(ErrorAlert)

export default PlayersFormError