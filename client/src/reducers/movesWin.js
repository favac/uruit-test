const movesWin = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVED_MOVES_WIN':
      return action.moves
    case 'SETTED_MOVE_WIN':
      return [...state, action.moveWin]  
    default:
      return state
  }
}

export default movesWin