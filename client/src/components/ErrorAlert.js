import React from 'react'

let ErrorAlert = ({error}) => (
  <div className="alert alert-danger" style={{display:error?'block':'none'}}>
    {error}
  </div>
)

export default ErrorAlert