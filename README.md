# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Programming assesment for UruIT

### How do I get set up? ###

* Clone this repo
* cd uruit-test 
* npm install 
* cd client
* npm run build 
* cd .. (back to uruit-test folder) 
* npm start
* App must be running in port 9000

### TEST
* npm jest (unit test for api)
* cd Client 
* npm test (tests for react)