const request = require('supertest')
const app = require("./app")

describe("Uruit assesment api", () => {
  describe ("GET /api/v1/moves - get all moves", () => {
    let expectedProps = ['id', 'name'];
    test("Should return array", () => {
      return request(app).get("/api/v1/moves")
      .expect(200)
      .then(res => {
        expect(res.body).toBeInstanceOf(Array)
      })
    })
    test('Should return objs with correct props', () => {
      return request(app).get('/api/v1/moves')
      .expect(200)
      .then(res => {
        // check for the expected properties
        let sampleKeys = Object.keys(res.body[0]);
        expectedProps.forEach((key) => {
          expect(sampleKeys.includes(key)).toBe(true);
        });
      });
    });
    test('shouldnt return objs with extra props', () => {
      return request(app).get('/api/v1/moves')
      .expect(200)
      .then(res => {
        // check for only expected properties
        let extraProps = Object.keys(res.body[0]).filter((key) => {
          return !expectedProps.includes(key);
        });
        expect(extraProps.length).toBe(0);
      });
    });
  })

  describe('POST api/v1/moves - Insert moves', () => {
    let badMove = {}
    test("Should reject info if not have name propertie", () => {
      return request(app).post('/api/v1/moves')
      .send(badMove)
      .then(res => {
        expect(res.status).toBe(400)
        expect(res.text.startsWith("Error:")).toBe(true)
      })
    })
    let move = {name: "Dog"}
    test("Should receive a valid object for move", () => {
      return request(app).post("/api/v1/moves")
      .send(move)
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.body).toBeInstanceOf(Object)
        expect(res.body.name).toBe("Dog")
        expect(res.body.id).toBe("dog")
      })
    })
  })

  describe("DELETE api/v1/moves - Delete move", () => {
    test("Should delete move by Id", () => {
      return request(app).delete("/api/v1/moves/dog")
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.text).toBe("dog")
        return request(app).get("/api/v1/moves")
      })
      .then(res => {
        let movesList =res.body
        expect(res.status).toBe(200)
        expect(movesList.findIndex(f => f.id === "dog")).toBe(-1)
      })
    })
  })

  describe("GET api/v1/players -  Both players info", () => {
    test("Should return object", () => {
      return request(app).get("/api/v1/players")
      .expect(200)
      .then(res => {
        expect(res.body).toBeInstanceOf(Object)
      })
    })
    let expectedProps = ['player1', 'player2'];
    test('Should return obj with correct props', () => {
      return request(app).get('/api/v1/players')
      .expect(200)
      .then(res => {
        // check for the expected properties
        let sampleKeys = Object.keys(res.body);
        expectedProps.forEach((key) => {
          expect(sampleKeys.includes(key)).toBe(true);
        });
      });
    });
  })
  describe("POST /api/v1/player - Insert Player", () => {
    const player = {
      player1: "Vanessa",
      player2: "Felix"
    }
    test("Should insert new Player and return object",  () => {
      return request(app).post("/api/v1/player")
      .send(player)
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.body).toBeInstanceOf(Object)
        return request(app).get("/api/v1/players")
      })
      .then(res => {
        let returned = res.body
        expect(res.status).toBe(200)
        expect(returned.player1).toBe("Vanessa")
        expect(returned.player2).toBe("Felix")
      })
    })
    const badPlayer = { player1: "player1", player2: " "}
    test("Should reject Player without name", () => {
      return request(app).post("/api/v1/reset")
      .then(res => {
        expect(res.status).toBe(200)
        return request(app).post("/api/v1/player")
        .send(badPlayer)
      })
      .then(res => {
        expect(res.status).toBe(400)
        expect(res.body.message.startsWith("Error:")).toBe(true)
      })
    })
  })

  describe("GET /api/v1/winner - Get the round winner", () => {
    let expectedProps = [
      'player',
      'playerName',
      'move',
      'round',
      'player1Score',
      'player2Score'
    ];
    test('Should receive player moves and return correct object winner', () => {
      return request(app).get("/api/v1/winner?player1=rock&player2=paper&round=1")
      .expect(200)
      .then(res => {
        // check for the expected properties
        let sampleKeys = Object.keys(res.body);
        expectedProps.forEach((key) => {
          expect(sampleKeys.includes(key)).toBe(true);
          expect(res.body.player).toBe("player2")
          expect(res.body.move).toBe("paper")
          expect(res.body.round).toBe("1")
          expect(res.body.player1Score).toBe(0)
          expect(res.body.player2Score).toBe(1)
        });
      });
    });
    test("Should return 'blank' object on tie", () => {
      return request(app).get("/api/v1/winner?player1=rock&player2=rock&round=1")
      .expect(200)
      .then(res => {
        expect(res.body.player).toBeNull()
        expect(res.body.move).toBeNull()
        expect(res.body.playerName).toBe("")
        expect(res.body.round).toBe("1")
      })
    })
    test("Should reject no complete query params", () => {
      return request(app).get("/api/v1/winner?player1:rock")
      .then(res => {
        expect(res.status).toBe(400)
        expect(res.text.startsWith("Error:"))
      })
    })
  })

  describe("GET /api/v1/rounds - return a list of rounds", () => {
    test("Should return a list of rounds", () => {
      return request(app).get("/api/v1/rounds")
      .expect(200)
      .then(res => {
        expect(res.body).toBeInstanceOf(Array)
      })
    })

    test("Should return 1 item on list of rounds, with correct properties", () => {
      expectedProps = ["round", "winner", "playerName"]
      return request(app).get("/api/v1/rounds")
      .expect(200)
      .then(res => {
        let sampleKeys = Object.keys(res.body[0]);
        expectedProps.forEach((key) => {
          expect(sampleKeys.includes(key)).toBe(true);
        });
        expect(res.body.length).toBe(1)
      })
    })
  })

  describe("POST /api/v1/reset - Reset player list and rounds", () => {
    test("Should reset list of players and rounds", () => {
      return request(app).post("/api/v1/reset")
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.body.message).toBe("ok")
        return request(app).get("/api/v1/rounds")
      })
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.body.length).toBe(0)
        // return request(app).get("/api/v1/players")
      })
      // .then(res => {
      //   let returned = res.body
      //   expect(res.status).toBe(200)
      //   expect(returned.player1).toBe("")
      //   expect(returned.player2).toBe("")
      // })
    })
  })

  describe("GET /api/v1/movesWin - Get list of possible move results", () => {
    let expectedProps = ["id", "move1", "move1Name", "move2", "move2Name", "win", "winName"]
    test("Should return list of moves", () => {
      return request(app).get("/api/v1/movesWin")
      .expect(200)
      .then(res => {
        expect(res.body).toBeInstanceOf(Array)
        expect(res.body.length).toBe(3)
        let sampleKeys = Object.keys(res.body[0]);
        expectedProps.forEach((key) => {
          expect(sampleKeys.includes(key)).toBe(true);
        });
      })
    })
    test('shouldnt return objs with extra props', () => {
      return request(app).get('/api/v1/movesWin')
      .expect(200)
      .then(res => {
        // check for only expected properties
        let extraProps = Object.keys(res.body[0]).filter((key) => {
          return !expectedProps.includes(key);
        });
        expect(extraProps.length).toBe(0);
      });
    });

  })
  describe("POST /api/v1/movesWin - Insert a possible result for moves", () => {
    test("Should insert new possible result for two moves", () => {
      let moveWin = {
        move1: "string",
        move2: "dog",
        win: "string"
      }
      return request(app).post("/api/v1/moves")
      .send({name: "Dog"})
      .then(res => {
        return request(app).post("/api/v1/moves")
        .send({name: "String"})
      })
      .then(res=>{
        return request(app).post("/api/v1/movesWin")
        .send(moveWin)
      })
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.body.move1).toBe("string")
        expect(res.body.move2).toBe("dog")
        expect(res.body.win).toBe("string")
      })
    })
    test("Should reject insert if not correct properties", () => {
      let badMoveWin = { move1: "", move2: "" }
      return request(app).post("/api/v1/movesWin")
      .send(badMoveWin)
      .then(res => {
        expect(res.status).toBe(400)
        expect(res.text.startsWith("Error: ")).toBe(true)
      })
    })
    test("Should reject insert if not empty properties", () => {
      let badMoveWin2 = { move1: "", move2: "", win: "" }
      return request(app).post("/api/v1/movesWin")
      .send(badMoveWin2)
      .then(res => {
        expect(res.status).toBe(400)
        expect(res.text.startsWith("Error: ")).toBe(true)
      })
    })
  })

  describe("DELETE /api/v1/movesWin - Delete a possible pair of moves", () => {
    test("Should delete item on movesWin with id", () => {
      return request(app).get("/api/v1/movesWin")
      .then(res => {
        expect(res.body.length).toBe(4)
        return request(app).delete("/api/v1/movesWin/4")
      })
      .then(res => {
        expect(res.status).toBe(200)
        expect(res.body.message).toBe("4")
        return request(app).get("/api/v1/movesWin")
      })
      .then(res => {
        expect(res.body.length).toBe(3)
      })
    })

  })



})