import React from 'react'
import { connect } from 'react-redux'
import { setPlayers, incrementRound, fetchMoves } from '../actions'
import PlayersFormError from './PlayersFormError'
import { Redirect } from 'react-router'

let PlayersForm = ({dispatch, players}) => {
  let player1Node, player2Node
  if (players.player1 !== "") {
    return <Redirect to="/playerMove/player1" />
  }

  return (
  <div className = "row">
    <div className="col-md-4 col-xs-1">
    </div>
    <div className="col-md-4 col-xs-10">
      <form onSubmit={ e => {
        e.preventDefault()
        dispatch(fetchMoves())
        dispatch(incrementRound())
        dispatch(setPlayers(player1Node.value, player2Node.value))
      }}>
        <div className="form-group">
          <label htmlFor="player1" className="control-label">Player 1</label>
          <input type="text" className="form-control" ref={node => player1Node = node }/>
        </div>
        <div className="form-group">
          <label htmlFor="player2" className="control-label">Player 2</label>
          <input type="text" className="form-control" ref={node => player2Node = node }/>
        </div>
        <PlayersFormError />
        <button type="submit" className="btn btn-success">Start</button>
      </form>
    </div>
    <div className="col-md-4 col-xs-1">
    </div>
  </div>
)}

const mapStateToProps = (state) => ({
  players: state.players
})
PlayersForm = connect(mapStateToProps)(PlayersForm)

export default PlayersForm