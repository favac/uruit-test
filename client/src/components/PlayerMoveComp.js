import React from 'react'
import ButtonMove from '../containers/ButtonMove'
import InfoRound from '../components/InfoRound'

const PlayerMoveComp = ({player, round, moves, match, history, players, winner}) => (
  <div>
    <div className="row">
      <div className="col-md-12">
        <InfoRound winner={winner} players={players} round={round}/>
      </div>
    </div>
    <div className="row">
      <div className="col-md-12" style={{textAlign:'center'}}>Select your move <b>{player}</b>:</div>
    </div>
    <div className="row">
      <div className="col-md-4 col-xs-1"></div>
      <div className="col-md-4 col-xs-10">
        { moves.map(m =>
          <ButtonMove move={m} key={m.id} playerId={match.params.player} history={history}/>
        )}
      </div>
      <div className="col-md-4 col-xs-1"></div>
    </div>
  </div>
)

export default PlayerMoveComp