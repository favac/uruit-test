const initialState = {
  player1: "",
  player2: ""
}

const players = (state = initialState, action) => {
  switch (action.type) {
    case 'RECEIVE_PLAYERS':
      return Object.assign({}, state, {
          player1: action.player1,
          player2: action.player2,
          error: null
      })
    case 'RECEIVE_PLAYERS_ERROR':
      return Object.assign({}, state, {
          error: action.error
      })
    default:
      return state
  }
}

export default players