const movesDelete = (state, id) => {
  let index = state.findIndex(f =>f.id === id)
  console.log("movesDelete index", index,state, id)
  return [
    ...state.slice(0, index),
    ...state.slice(index + 1)
  ]
}

const moves = (state = [], action) => {
  switch (action.type) {
    case 'REQUEST_MOVES':
      return state
    case 'RECEIVE_MOVES':
      return action.moves
    case 'SETTED_MOVE':
      return [...state, action.move]
    case 'REMOVED_MOVE':
      return movesDelete(state, action.id)
    default:
      return state
  }
}
export default moves